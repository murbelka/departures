require '../config/environment.rb'
require 'net/http'
require 'uri'
require 'csv'    
require 'date'


def open(url)
  Net::HTTP.get(URI.parse(url))
end

departures_csv = open('http://developer.mbta.com/lib/gtrtfs/Departures.csv')

#### if there's a problem with getting the CSV online, uncomment the string below ####
# departures_csv = File.read('Departures.csv')

csv = CSV.parse(departures_csv, :headers => true)

Departure.delete_all unless csv.nil? || csv.empty?

csv.each do |row|
	Departure.new(
		:time_stamp => Time.at(row.to_hash['TimeStamp'].to_i).to_datetime,
		:origin => row.to_hash['Origin'],
		:trip => row.to_hash['Trip'],
		:destination => row.to_hash['Destination'],
		:scheduled_time => Time.at(row.to_hash['ScheduledTime'].to_i).to_datetime,
		:lateness => row.to_hash['Lateness'],
		:track => row.to_hash['Track'],
		:status => row.to_hash['Status']
	).save
end



