class CreateDepartures < ActiveRecord::Migration
  def change
    create_table :departures do |t|
      t.timestamp :time_stamp
      t.string :origin
      t.integer :trip
      t.string :destination
      t.timestamp :scheduled_time
      t.integer :lateness
      t.integer :track
      t.string :status

      t.timestamps null: false
    end
  end
end
