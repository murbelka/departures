class ChangeIntToTimestamp < ActiveRecord::Migration
  def change
  	change_column :departures, :time_stamp, :timestamp
  	change_column :departures, :scheduled_time, :timestamp
  end
end
